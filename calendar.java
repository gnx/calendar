import java.net.URL;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.awt.event.ActionEvent;
public class Calendar {
	public static void main(String[] args) {
		
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
	    frame.setSize(646, 267);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setContentPane(panel);
	    panel.setLayout(null);

	    JLabel imagelabel = new JLabel();
        URL imageUrl = Calendar.class.getResource("calendar.png");
        ImageIcon imageTriangle = new ImageIcon(imageUrl);
        imagelabel.setIcon(imageTriangle);
        imagelabel.setBounds(403,0,217,233);
        panel.add(imagelabel);
        
        JLabel lblNewLabel = new JLabel("Година");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setBounds(10, 22, 112, 24);
        panel.add(lblNewLabel);
        
        JLabel lblNewLabel_1 = new JLabel("Месец");
        lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel_1.setBounds(8, 51, 112, 24);
        panel.add(lblNewLabel_1);
        
        JLabel lblNewLabel_2 = new JLabel("Ден");
        lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel_2.setBounds(29, 79, 61, 24);
        panel.add(lblNewLabel_2);
        
        JTextField textG = new JTextField();
        textG.setBounds(114, 24, 86, 20);
        panel.add(textG);
        textG.setColumns(10);
        
        JTextField textM = new JTextField();
        textM.setBounds(114, 53, 86, 20);
        panel.add(textM);
        textM.setColumns(10);
        
        JTextField textD = new JTextField();
        textD.setBounds(114, 83, 86, 20);
        panel.add(textD);
        textD.setColumns(10);
        
        JButton btnNewButton = new JButton("Какво се е случило на тази дата?");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String strG = textG.getText();
                String strM = textM.getText();
                String strD = textD.getText();

                 int G = Integer.parseInt(strG);
                 int M = Integer.parseInt(strM);
                 int D = Integer.parseInt(strD);
                 
                 
                 try {
                	 if (G > 2021) {
                		 JOptionPane.showMessageDialog(frame, "Не трябва годината да е по-голяма от 2021", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (G <= 0) {
                	  JOptionPane.showMessageDialog(frame, "Не трябва годината да е по-малка или равна на 0", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (M <= 0) {
                	  JOptionPane.showMessageDialog(frame, "Не трябва месеца да е по-малък или равен на 0", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (M > 12) {
                	 JOptionPane.showMessageDialog(frame, "Не трябва месеца да е по-голям от 12", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (D <= 0) {
                	 JOptionPane.showMessageDialog(frame, "Не трябва деня да е по-малък или равен на 0", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (M == 1 || M == 3 || M == 5 || M == 7 || M == 8 || M == 10 || M == 12 && (D > 31 || D<= 0 )) {
                	 JOptionPane.showMessageDialog(frame, "Този месец има 31 дена", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if (M == 4 || M == 6 || M == 9 || M == 11 && (D > 30 || D <= 0 )) {
                	 JOptionPane.showMessageDialog(frame, "Този месец има 30 дена", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if ((G%4 == 0 && (G%10 != 0)) || (G%400 == 0) && M == 11 && (D > 29 || D <= 0 )) {
                	 JOptionPane.showMessageDialog(frame, "Тaзи година е високосна", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if (!((G%4 == 0) && (G%100 != 0)) || (G%400 == 0) && M == 11 && (D > 28 || D <= 0 )) {
                	 JOptionPane.showMessageDialog(frame, "Тaзи година не е високосна", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if (strM.equals(null)){
                	  JOptionPane.showMessageDialog(frame, "Тaзи година не е високосна", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 }
                	 
                	 File reader = new File("filename.txt");
                     Scanner myReader = new Scanner(reader); 
                     while (myReader.hasNextLine()) {
                         String data = myReader.nextLine();
                         if(data.contains(G + "/" +  M  + "/" + D)) {
                        		JOptionPane.showMessageDialog(frame, ""+ data, "", JOptionPane.INFORMATION_MESSAGE); 
                         }
                     }
                     myReader.close();
                 } catch (FileNotFoundException f) {
                	 JOptionPane.showMessageDialog(frame, "Въведете дата!", "", JOptionPane.INFORMATION_MESSAGE); 
                   
                 }
        	}
        	
        });
        btnNewButton.setBackground(Color.WHITE);
        btnNewButton.setBounds(29, 131, 232, 52);
        panel.add(btnNewButton);
	        
	    frame.setVisible(true);
	        
	}
}
